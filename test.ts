export default [
    {
        type: 'POST',
        route: '/api/:id',
        response: {
            status: 200,
            body: {
                success: true,
                data: {
                    id: 1,
                    name: 'Vadim',
                },
            },
        },
    },
    {
        type: 'GET',
        route: '/api',
        response: {
            status: 200,
            body: {
                success: true,
                data: {
                    id: 1,
                    name: 'Vadim',
                },
            },
        },
    },
];